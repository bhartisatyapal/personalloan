import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import Header from '../container/Header/Header';
import Home from '../container/Home/Home';
import Footer from '../container/Footer/Footer';
import '../assets/css/global.css';


import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Home />
        <Footer/>
      </div>
    );
  }
}

export default App;
