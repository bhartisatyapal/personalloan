import React from 'react';

const Loader=()=>{
    return(
        <div className="loader_div" style={{display:'block'}}>
                                <div className="LoaderBalls">
                                    <div className="LoaderBalls__item"></div>
                                    <div className="LoaderBalls__item"></div>
                                    <div className="LoaderBalls__item"></div>
                                </div>
        </div>
    )
}
export default Loader;