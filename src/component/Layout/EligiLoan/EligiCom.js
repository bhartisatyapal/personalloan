import React from 'react';

const EligiCom=(props)=>{
    return(
        <React.Fragment>
            <li>
            <i className={`${props.iconeli} icons_cm`}></i>
            <div className="heading">{props.heading}</div>
               {props.detail} 
            </li>
        </React.Fragment>
    )
}
export default EligiCom;