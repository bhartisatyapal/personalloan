import React from 'react';
import ElLoan from './EligiCom';
import './EligiLoan.css';

const EligiLoan=(props)=>{
    return(
        <section className="section_container mfland_page section_third">
            <div className="row">
                <div className="section_first">
                    <div className="col-md-12">
                     <h2 className="common_heading">Factors affecting your Eligibility For A Personal Loan</h2>
                    </div>
                    <div className="row">
                        <div className="types_personal_loan">
                            <ul>
                                <ElLoan iconeli="monthly_income"
                                 heading="Monthly Income" detail="
                                    Individuals with higher income and job stability have a higher likelihood of repaying the loan.
                                " />
                               
                                 <ElLoan iconeli="age"
                                 heading="Age" detail="
                                    The borrower’s age plays an important role in the ability of repayment of the debt. 
                                " />
                                 <ElLoan iconeli="monthly_expenses"
                                 heading="Monthly Expenses" detail="
                                    Your ability to pay back a loan is judged to some extent from your monthly expense, making it an important factor for evaluation.
                                " />
                                <ElLoan iconeli="any_loan"
                                 heading="Any Other Loan" detail="When it comes to Loans, it’s not more the merrier. Your chances improve if you don’t have another loan to your name." />

                                <ElLoan iconeli="credit_history"
                                 heading="Credit History" detail="A bad CIBIL Score is the last thing you would want if you are applying for a Loan. You should always maintain a credit history.
                                " />  

                                <ElLoan iconeli="job_stability"
                                 heading="Job Stability" detail="Job stability plays a significant role in your loan, If you’re in the habit of frequently switching your jobs, your loan application might be rejected.
                                " />  
                                 <ElLoan iconeli="relation_lender"
                                 heading="Relationship with Lender" detail="A good understanding with the bank goes a long way in negotiating better terms for a loan. Being a known and trusted customer helps.
                                " />  

                                
                                <ElLoan iconeli="category_employer"
                                 heading="Category of Your Employer" detail="The reputation of an applicant's workplace is also a contributing factor to the determination of interest rate.
                                " />  



                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default EligiLoan;