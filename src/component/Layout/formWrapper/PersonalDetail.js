import React from 'react';

const personalDetail =(props)=>{
   // console.log(props.input_field);
   

    return(
        <div className="row mar_btm60">
        <div className="sub_heading">Enter Your Personal Details</div>
        
                        <div className="col-md-3">
                            <div className="form_field">
                                <label className="label-text" >Email ID</label>
                               <input type="text" className="form-control placeholder-text" onBlur={props.emailBlur} refs="name" value={props.datastate["email"]||''} onChange={(e)=>props.changefunction(e,"email")} />
                                <span className="error-lbl">{props.dataerror["email"]}</span>
                              </div>  
                        </div>																								
                        
                        <div className="col-md-3">
                            <div className="form_field">
                                <label className="label-text">Mobile Number</label>
                                <input type="text" className="form-control placeholder-text validate" value={props.datastate["txtMobile"]} onChange={(e)=>props.datastate(e,"txtMobile")}/>
                                <span className="error-lbl" style={{display:'none'}}>{props.dataerror["txtMobile"]}</span>
                               </div> 
                            </div>	
                        
                        <div className="col-md-3">
                            <div className="form_field">
                                <label className="label-text" >Enter OTP</label>
                                <span className="resend_otp_pl">Resend OTP</span>
                                <input type="text" className="form-control placeholder-text"  id="mob_otp"/>
                                <span className="error-lbl" style={{display:'none'}}>Error message</span>
                            </div>	
                        </div>
                        <div className="col-md-3">
                            <div className="form_field">
                                <label className="label-text" >Full Name</label>
                                <input type="text" className="form-control placeholder-text validate" id="txtFullname"/>
                                <span className="error-lbl" style={{display:'none'}}>Error message</span>
                            </div>
                          </div>  	

                        <div className="col-md-3">
                            <div className="form_field">
                                <label className="label-text" >Pincode</label>
                                <input type="text" className="form-control placeholder-text validate" id="txtPincode"/>
                                <span className="error-lbl" style={{display:'none'}}>Error message</span>
                            </div>	
                        </div>

                        <div className="col-md-3">
                            <div className="form_field">
                                <label className="label-text" >City</label>
                                <input type="text" className="form-control placeholder-text validate disablecl" id="txtCity" readOnly=""/>

                            </div>	
                        </div>  
                    
                    
    </div>

    )
}
   


export default personalDetail;