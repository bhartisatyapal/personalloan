import React from 'react';
import {Typeahead} from 'react-bootstrap-typeahead';

const SalaryForm=(props)=>{

return(
<div className="salaried_div mar_btm60">

  
  <div className="row">
  <div className="col-md-12"><div className="sub_heading">Employer &amp; Loan Requirement Details</div></div>
    <div className="col-md-3">
      <div className="form_field">
        <label className="label-text" >Required Loan Amount</label>
        <input tabIndex="10" type="tel" maxLength="10" className="form-control placeholder-text validate only_d num_comma validate-icon" id="txtSalariedloanamount" name="txtloanamount"/>
        <span className="error-lbl" id="txtSalariedloanamount_err" style={{display:'none'}}></span>
      </div>
    </div>
    <div className="col-md-3">
      <div className="form_field">
        <label className="label-text">Current Company Name</label>
        <Typeahead 
        labelKey={(option) => `${option.firstName} ${option.lastName}`}
        options={[
          {firstName: 'Art', lastName: 'Blakey'},
          {firstName: 'John', lastName: 'Coltrane'},
          {firstName: 'Miles', lastName: 'Davis'},
          {firstName: 'Herbie', lastName: 'Hancock'},
          {firstName: 'Charlie', lastName: 'Parker'},
          {firstName: 'Tony', lastName: 'Williams'},
        ]}
        placeholder="Current Company Name"
      />   
    </div>
    </div>
    <div className="col-md-3">
      <div className="form_field">
        <label className="label-text">Organization Category</label>
        <select className="form-control placeholder-text">
            <option value="Public Ltd">Public Ltd</option>
            <option value="Private Ltd">Private Ltd</option>
            <option value="MNC">MNC</option>
            <option value="Central/State Govt">Central/State Govt</option>
            <option value="Proprietorship">Proprietorship</option>
            <option value="LLP">LLP</option>
            <option value="Partnership firm">Partnership firm</option>
            <option value="Others">Others</option>
            
        </select>
        <div className="lbl">
          <span className="error-lbl" id="txtSalaryOrganizationCategory_err" style={{display:'none'}}></span>
        </div>
      </div>
    </div>
    <div className="col-md-3">
      <div className="form_field">
        <label className="label-text">Salary Bank Account</label>
        <select className="form-control placeholder-text">
            <option value="HDFC Bank">HDFC Bank</option>
            <option value="SBI Bank">SBI Bank</option>
            <option value="AXIS Bank">AXIS Bank</option>
            <option value=" ICICI Bank"> ICICI Bank</option>
            <option value="CITI Bank">CITI Bank</option>
            <option value="Other Bank">Other Bank</option>
            <option value="Cash / Cheque">Cash / Cheque</option>
           
            
        </select>
       
        <div className="lbl">
          <span className="error-lbl" id="txtSalaryBank_err" style={{display:'none'}}></span>
        </div>
      </div>
    </div>
  </div>
  <div className="row">
    <div className="col-md-3">
      <div className="form_field">
        <label className="label-text" >Net Monthly Income</label>
        <input tabIndex="14" type="tel" maxLength="10" className="form-control placeholder-text validate only_d num_comma validate-icon" id="txtNetincome" name="txtNetincome" placeholder="Post tax salary" />
        <span className="error-lbl" id="txtNetincome_err" style={{display:'none'}}></span>
      </div>
    </div>
  </div>
</div>
)
}
export default SalaryForm;
