import React from 'react';
import {Typeahead} from 'react-bootstrap-typeahead';

const SelftEmp=(props)=>{
    console.log(props.radiovalue);
    return(
        <div className="self_employed_business_div">
            <div className="col-md-12">
                <div className="sub_heading">Enter Your Business and Loan Requirement Details</div>
            </div>
            <div className="row">
                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Required Loan Amount</label>
                        <input tabIndex="10" type="tel" className="form-control placeholder-text validate only_d num_comma validate-icon" maxLength="10" name="txtLoanAmt" />
                        <span className="error-lbl" id="txtSelfemployedLoanamount_err" style={{display:'none'}}></span>
                    </div>	
                </div>
                {props.radiovalue==="selfEmpB"?
                <React.Fragment>
                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Employement Role</label>
                        <select className="form-control placeholder-text">
                            <option value="Private Ltd">Private Ltd</option>
                            <option value="Partnership Company">Partnership Company</option>
                            <option value="Central/State Govt">Proprietorship Company</option>
                        </select>

                    </div>
                </div>
                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Industry</label>
                        <select className="form-control placeholder-text">
                            <option value="Manufacturing">Manufacturing</option>
                            <option value="Engineering">Engineering</option>
                            <option value="Trading">Trading</option>
                            <option value="Services">Services</option>
                            <option value="Services"> Others</option>
                        </select>
                </div>
                </div></React.Fragment>:
                <React.Fragment>
                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Profession</label>
                        <select className="form-control placeholder-text">
                            <option value="Charactered Account">Charactered Account</option>
                            <option value="Doctor">Doctor</option>
                            <option value="Lawyer">Lawyer</option>
                            <option value="Architect">Architect</option>
                            <option value="Other">Other</option>
                        </select>

                    </div>
                </div>
                </React.Fragment>}
                

                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">IT Returns Filed</label>
                        <select className="form-control placeholder-text">
                        <option value="Less than or equal to 2years">Less than or equal to 2years</option>
                        <option value="Greater than 2years">Greater than 2years</option>
                    </select>

                    </div>
                </div>

                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Annual Turn Over</label>
                        <input tabIndex="14" type="tel" maxLength="13" className="form-control placeholder-text validate only_d num_comma validate-icon" name="txtAnnualturnoverincome" id="txtAnnualturnoverincome"/>

                    </div>
                </div>


                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Residence Type</label>
                        <select className="form-control placeholder-text">
                            <option value="Owned by Self/Spouse">Owned by Self/Spouse</option>
                            <option valur="Owned by Parent/Sibling">Owned by Parent/Sibling</option>
                            <option value="Rented -Staying Alone">Rented -Staying Alone</option>
                            <option value="Rented - With Family"> Rented - With Family</option>
                            <option value="Rented - With Friends">Rented - With Friends</option>
                            <option value="Company Provided">Company Provided</option>
                            <option value="Hostel">Hostel</option>
                            <option value="Paying Guest">Paying Guest</option>
                    </select>

                    </div>
                </div>
                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Residing Since (Years)</label>
                        <input tabIndex="16" type="tel" maxLength="2" className="form-control placeholder-text validate only_d validate-icon" name="txtSelfemployedRSince" id="txtSelfemployedRSince" />
                 

                    </div>
                </div>
                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Office City</label>
                        <Typeahead 
                        labelKey={(option) => `${option.city}`}
                        options={[
                        {city: 'Mumbai'},
                       
                        ]}
                        placeholder="Current Company Name"
                        />   
                 

                    </div>
                </div>

                <div className="col-md-3">
                    <div className="form_field">
                        <label className="label-text">Office Type</label>
                        <select className="form-control placeholder-text">
                            <option value="Owned">Owned</option>
                            <option valur="Rented">Rented</option>
                    </select>
                 

                    </div>
                </div>



            </div>
            
        </div>
        //self_employed_business_div
    )
} 
export default SelftEmp;



