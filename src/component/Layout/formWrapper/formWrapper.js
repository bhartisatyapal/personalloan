import React,{Component} from 'react';
import PersonalDetail from './PersonalDetail';
import SalaryForm from './SalaryForm';
import FormSelft from './SelfEmployed';
import Loader from '../Loader/Loader';
import './formWrapper.css'

class FromWrapper extends Component{
    constructor(props){
        super(props);
        this.state={
            selectOpton:'salary',
            input_field:{},
            error:{},
        }
        this.handelchange = this.handelchange.bind(this);
    }

    radioButtonHandler=(e)=>{
        let getRadiobtn_value=e.currentTarget.value;
        this.setState({selectOpton:getRadiobtn_value});
    }

    handelchange=(e,data)=>{

       let field_var = this.state.input_field;
       field_var[data] = e.target.value;
       this.setState({field_var})
        //console.log(data)
        
    }
    Email_validatar=(email)=>{
        let regexEmail = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;
        return regexEmail.test(email);
    }

    emailBlurHandler=()=>{
        let errors={}
        let emailvali=true;
        let emailcheck= this.state.input_field['email'];
        //console.log(emailcheck);
        //debugger
        if(emailcheck==="" || typeof emailcheck==="undefined"){
            emailvali=false;
            errors["email"]="please enter Email";
            
        }else if(!this.Email_validatar(emailcheck)){
            emailvali=false;
            errors["email"]="please enter valid  Email ID";
        }
        else{
            emailvali=true;
            errors["email"]="";

        }
        this.setState({error:errors});
        return emailvali;
        
    }

    componentDidMount(){
        //this.emailBlurHandler();
    }

    render(){
     
        
        let divOption=<Loader/>;
        if(this.state.selectOpton==='salary'){
            divOption=<SalaryForm/>
        }
        else{
            divOption=<FormSelft radiovalue={this.state.selectOpton}/>
        }

        return(
            <div className="section_container mfland_page topbanner">
                <div className="row clearfix">
                    <div className="section_first">
                        
                            <h2 className="common_heading">Apply for a Personal Loan</h2>
                            <div className="col-md-12">
                            <div className="sub_heading">Select Your Occupation</div>
                            </div>
                            
                            <div className="row mar_btm60">
                                <div className="col-md-3">
                                    <div className="form_field occupation">
                                        <label className="container1">Salaried
                                            <input onChange={this.radioButtonHandler} type="radio" value="salary" name="occupation" id="salaried" defaultChecked />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>																								
                                </div>
                                <div className="col-md-3">
                                    <div className="form_field occupation active">
                                        <label className="container1">Self Employed Business
                                            <input onChange={this.radioButtonHandler} type="radio" value="selfEmpB" name="occupation" id="self_employed_business" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>		
                                </div>
                                <div className="col-md-3">
                                    <div className="form_field occupation">
                                        <label className="container1">Self Employed Professional
                                            <input onChange={this.radioButtonHandler} type="radio" value="selfEmpP" name="occupation" id="self_employed_professional" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>		
                                </div>
                            </div>
                            <PersonalDetail emailBlur={this.emailBlurHandler} datastate={this.state.input_field} dataerror={this.state.error} changefunction={this.handelchange}/> 
                           
                          {divOption}
                          
                         
                          
    
                    </div>
                    <div style={{clear:'both'}}></div>
                    <div className="clearfix" style={{textAlign:'center',marginTop:'50px'}}>
                    <input type="button"  className="btn-sec2 btnhgt" value="Submit Loan Application"/>
                            <p className="tnc">By clicking on "Submit" you hereby accept the Terms &amp; Conditions and Declaration for MyUniverse Account. Your personal loan application will be shared with Banks if you meet bank's eligibility criteria.</p>
                    </div>
                    
                </div>
            </div> 
        )
    }
    
}

export default FromWrapper;