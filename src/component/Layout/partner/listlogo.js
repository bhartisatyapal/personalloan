import React from 'react';

const listlogo=(props)=>{
    return(
        <span className="partners_logo">
            <img src={require(`./images/${props.imagename}`)} alt="Aditya Birla"/>
        </span>
    )
}

export default listlogo;