import React from 'react';
import Listlogo from './listlogo';
import './partner.css';

const partner=(props)=>{
    return(
        <section className="section_container mfland_page section_third">
            <div className="row">
                <div className="section_first">
                    <h2 className="common_heading">List of Partners</h2>
                    <div className="list_of_partners">
                    
                    <Listlogo imagename="adityabirla_logo.jpg"/>
                    <Listlogo imagename="citi_logo.jpg"/>
                    <Listlogo imagename="edelwise_logo.jpg"/>
                    <Listlogo imagename="logo-fullertion.jpg"/>
                    <Listlogo imagename="logo-hdbfs.jpg"/>
                    <Listlogo imagename="logo-hdfc.jpg"/>
                    <Listlogo imagename="logo-icici.jpg"/>
                    <Listlogo imagename="paysense_logo.jpg"/>
                    </div>
                </div>
             </div>
         </section>       
    )
}
export default partner;