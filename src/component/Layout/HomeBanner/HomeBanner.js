import React from 'react';
import './HomeBanner.css';

const HomeBanner =(props)=>{
    return(
        <section className="hero-a-banner">
            <div className="container-fluid  abc-hero">
            <div className="section_container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="hero_content">
                            <h1 className="scnd-txt">
                                Don't wait to<br/>realize your dreams
                            </h1>	

                            <a className="btn-sec2 applynowbtn" href="#section2">Apply Now</a>
                          </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    )
}
export default HomeBanner;