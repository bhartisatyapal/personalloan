import React from 'react';
import './Benefit.css';
import Leftpanel from './Leftpanel/leftpanel';
import Rightpanel from './Rightpanel/Rightpanel';

const benefit=(props)=>{
    return(
        <section className="section_container mfland_page section_two">
            <div className="row">
                <div className="section_first">
                    <div className="col-md-12">
                        <h2 className="common_heading">Benefits of Personal Loan </h2>
                    </div>
                    <div className="row benefits_wrapper">
                        <div className="col-md-6 first_row">
                            <Leftpanel imageclass="door_services" title="Doorstep services" desc="No more visiting to multiple branches to fulfil your requirement. Myuniverse helps you to compare 5+ personal loan providers at your doorstep. "/>

                             <Leftpanel imageclass="loan_sanction" title="Loan Sanction Period" desc="Apply for personal loan and get the disbursement within 3 working days*(Subject to availability, complete documentation, etc.)."/>

                             <Leftpanel imageclass="balance_tranfer" title="Balance Transfer" desc="Opt for personal loan balance transfer and avail benefits like Top up personal loan, special interest rate starting at 10.99%* & many more. "/>
                             <Leftpanel imageclass="balance_tranfer" title="Pre-closure & Processing charges" desc="Depending on your loan profile, you can also avail the minimum pre-closure and processing fees charges which will help you to save more while applying for personal loan. "/>
                        </div>
                        <div className="col-md-6 second_row">
                            <Rightpanel/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default benefit;