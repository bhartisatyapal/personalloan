import React from 'react';
import './leftpanel.css';

const leftpanle=(props)=>{
    return(
        <div className="benefits_main">
        <div className="benefits_icon ">
        <i className={`${props.imageclass} icons_cm`}></i>
        </div>
        <div className="benefits_text">
        <div className="hd">{props.title}</div>
        <p>{props.desc}</p>
        </div>
        </div>
    )
}
export default leftpanle;