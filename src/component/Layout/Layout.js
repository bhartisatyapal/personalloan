import React,{Component} from 'react';
import HomeBanner from './HomeBanner/HomeBanner';
import FormWrap from './formWrapper/formWrapper';
import Benefit from './BenefitSec/Benefit';
import EligiLoan from './EligiLoan/EligiLoan';
import Partner from './partner/partner';

class Layout extends Component{
    
    render(){
        return(
            <div>
                <HomeBanner/>
                <FormWrap/>
                <Benefit/>
                <EligiLoan/> 
                 <Partner/>

            </div>
        )
    }
}

export default Layout;